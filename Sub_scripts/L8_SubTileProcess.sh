#!/bin/bash
## Processing Landsat 8 data per scene
## Author: Sajid Pareeth, 2017
## For 15m pan, use band - 8
## For 30m, use bands - 1,2,3,4,5,6,7
## Note: S2A Band 8A = L8 Band 5 (NIR)

cleanup()
{
  m=$1
  # we clean up in the GRASS DB and the scratch space:
  g.remove type=rast name=${m}_B1,${m}_B2,${m}_B3,${m}_B4,${m}_B5,${m}_B6,${m}_B7,${m}_B8,${m}_B9 -f
}

# what to do in case of user break:
exitprocedure()
{
 g.message -e 'User break!'
 unset GRASS_BATCH_JOB
 cleanup ${BASE}
 rm -rf ${GRASSDB}/L8_UTM_${CODE}
 exit 1
}
# shell check for user break (signal list: trap -l)
trap "exitprocedure" 2 3 15


if [ -z "$GISBASE" ] ; then
    echo "Please run L8_TileProcess_v2.sh to run this program." >&2
    exit 1
fi
export GRASS_OVERWRITE=1
export GRASS_MESSAGE_FORMAT=plain  # percent output as 0..1..2..
# setting environment, so that awk works properly in all languages
unset LC_ALL
LC_NUMERIC=C
export LC_NUMERIC
mkdir -p ${OUTDIR}/${T}/INDICES
mkdir -p ${OUTDIR}/${T}/COMPOSITES
mkdir -p ${OUTDIR}/${T}/SPECTRAL
cd ${INDIR}/${T}

for FOLDER in `ls`; do
	cd ${INDIR}/${T}/${FOLDER}
	BASE=${FOLDER}
	if [[ ! -z ${DISTRICT} ]]; then
		VOUT=`echo ${DISTRICT}|rev|cut -d/ -f1|rev|cut -d. -f1`
		v.in.ogr input=${DISTRICT} output=${VOUT}
		g.region vect=${VOUT} res=30 -a
		r.mask vect=${VOUT}
	else
		echo "Processing the entire tile"
		r.in.gdal input=${BASE}_B1.TIF output=${BASE}_B1 memory=1000 -e
		g.region rast=${BASE}_B1 res=30 -a
	fi

	##importing all the bands
    for i in "2" "3" "4" "5" "6" "7" "8" "9" "QA"; do
		r.in.gdal input=${BASE}_B${i}.TIF output=${BASE}_B${i} memory=1000
	done
	#g.region rast=${BASE}_B1 res=30 -a
	i.landsat.toar input=${BASE}_B output=${BASE}_toar metfile=${BASE}_MTL.txt sensor=oli8
	#convert toar into integer
	for i in "1" "2" "3" "4" "5" "6" "7"; do
		r.mapcalc "${BASE}_toar${i} = int(${BASE}_toar${i} * 10000)"
	done
	#data fusion:
	g.region rast=${BASE}_toar1 res=15 -a
	r.mapcalc "${BASE}_toar8 = int(${BASE}_toar8 * 10000)" --o
	i.fusion.hpf -l -c pan=${BASE}_toar8 msx=${BASE}_toar1,${BASE}_toar2,${BASE}_toar3,${BASE}_toar4,${BASE}_toar5,${BASE}_toar6,${BASE}_toar7 center=high modulation=max trim=0.0 --o
	#create composites
	i.colors.enhance red="${BASE}_toar4.hpf" green="${BASE}_toar3.hpf" blue="${BASE}_toar2.hpf" strength=95
	r.composite red="${BASE}_toar4.hpf" green="${BASE}_toar3.hpf" blue="${BASE}_toar2.hpf" output="${BASE}_toar.hpf_comp_432"
	i.colors.enhance red="${BASE}_toar5.hpf" green="${BASE}_toar4.hpf" blue="${BASE}_toar3.hpf" strength=95
	r.composite red="${BASE}_toar5.hpf" green="${BASE}_toar4.hpf" blue="${BASE}_toar3.hpf" output="${BASE}_toar.hpf_comp_543"
	#cloud masking
	# add 28672 and 31744, which are cirrus, Note, the pixel 1 is background in BQA, may be use that as mask while exporting to avoid big data
	r.mapcalc "${BASE}_cloudmask = if((${BASE}_BQA == 1 || ${BASE}_BQA == 61440 || ${BASE}_BQA == 59424 || ${BASE}_BQA == 57344 || ${BASE}_BQA == 56320 || ${BASE}_BQA == 53248 || ${BASE}_BQA == 31744 || ${BASE}_BQA == 28672), null())"
	r.mask -r
	r.mask ${BASE}_cloudmask
	r.mapcalc "${BASE}_NDVI = (${BASE}_toar5.hpf - ${BASE}_toar4.hpf) / (${BASE}_toar5.hpf + ${BASE}_toar4.hpf) * 1.0"
	r.mapcalc "${BASE}_NDWI = (${BASE}_toar5.hpf - ${BASE}_toar6.hpf) / (${BASE}_toar5.hpf + ${BASE}_toar6.hpf) * 1.0"
	for i in "1" "2" "3" "4" "5" "6" "7"; do
		r.mapcalc "${BASE}_toar${i}.hpf_masked = ${BASE}_toar${i}.hpf" --o
	done
	r.mask -r
	i.group group=${BASE}_spectral input=${BASE}_toar2.hpf,${BASE}_toar3.hpf,${BASE}_toar4.hpf,${BASE}_toar5.hpf,${BASE}_toar6.hpf,${BASE}_toar7.hpf
	i.group group=${BASE}_spectral_masked input=${BASE}_toar2.hpf_masked,${BASE}_toar3.hpf_masked,${BASE}_toar4.hpf_masked,${BASE}_toar5.hpf_masked,${BASE}_toar6.hpf_masked,${BASE}_toar7.hpf_masked
	r.out.gdal in=${BASE}_spectral out=${OUTDIR}/${T}/SPECTRAL/${BASE}_spectral.tif
	r.out.gdal in=${BASE}_spectral_masked out=${OUTDIR}/${T}/SPECTRAL/${BASE}_spectral_masked.tif
	r.out.gdal in=${BASE}_toar.hpf_comp_432 out=${OUTDIR}/${T}/COMPOSITES/${BASE}_comp_432.tif
	r.out.gdal in=${BASE}_toar.hpf_comp_543 out=${OUTDIR}/${T}/COMPOSITES/${BASE}_comp_543.tif
	r.out.gdal in=${BASE}_NDVI out=${OUTDIR}/${T}/INDICES/${BASE}_NDVI.tif
	r.out.gdal in=${BASE}_NDWI out=${OUTDIR}/${T}/INDICES/${BASE}_NDWI.tif
	rm -f ${OUTDIR}/${T}/CloudReport_${T}.txt
	CLD=`r.stats ${BASE}_cloudmask -p|tail -1|cut -d' ' -f2`
	echo "${BASE},${CLD}" >> ${OUTDIR}/${T}/CloudReport_${T}.txt
	cleanup ${BASE}
done
MAPS=`g.list rast map=. pattern=LC8*NDVI$`
CNT=`g.list rast map=. pattern=LC8*NDVI$|wc -l`
rm -f ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "A total of ${CNT} datasets were processed for the tile ${T}." >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "All the bands were converted to TOA reflectance." >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "TOAR is quantized by a factor of 10000." >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "Bands 1 to 7 were fused to 15m, and further processing were done at 15m" >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "Cloud mask is based on the provided BQA band. For the extent of cloud in each scene please see CloudReport_${T}.txt" >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "The folder SPECTRAL consists of two tif files, 1) Stack of bands 2,3,4,5,6,7 before applying cloud mask 2) Stack of bands 2,3,4,5,6,7 before applying cloud mask" >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "The tif files in the 'SPECTRAL' folder will be used as input to Classification step" >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "The 'INDICES' folder has NDVI and NDWI of each scene in tif format" >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "The 'COMPOSITES' folder has true color (432), and False color (543) composites in tif format" >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "The following scenes are processed for the path/row ${T}:" >> ${OUTDIR}/${T}/Report_processing_${T}.txt
echo "${MAPS}" >> ${OUTDIR}/${T}/Report_processing_${T}.txt
