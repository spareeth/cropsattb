#!/bin/bash
## Author: Sajid Pareeth, 2017
## Script to develop multi linear model and estimate yield

# what to do in case of user break:
exitprocedure()
{
 g.message -e 'User break!'
 unset GRASS_BATCH_JOB
 rm -rf ${TMP} ${GRASSDB}/TMP_${CODE}
 exit 1
}
# shell check for user break (signal list: trap -l)
trap "exitprocedure" 2 3 15


if [ -z "$GISBASE" ] ; then
    echo "Please run G_ModelYield.sh to run this program." >&2
    exit 1
fi
export GRASS_OVERWRITE=1
export GRASS_MESSAGE_FORMAT=plain  # percent output as 0..1..2..
# setting environment, so that awk works properly in all languages
unset LC_ALL
LC_NUMERIC=C
export LC_NUMERIC

OUTY=`echo ${Y}|cut -d. -f1`
v.in.ogr input=${INDIR}/${Y} output=${OUTY}
IN=`echo ${XI}|sed 's/,/ /g'`
for i in ${IN}; do
	OUT=`echo ${i}|cut -d. -f1`
	r.in.gdal input=${INDIR}/${i} output=${OUT}
	g.region rast=${OUT}
done
MAPS=`g.list rast map=. sep=,`
v.to.rast input=${OUTY} output=${OUTY} use=attr attribute_column=${VAR}
r.regression.multi mapx=${MAPS} mapy=${OUTY} residuals=${PREFIX}_residual estimates=${PREFIX}_estimate output=${INDIR}/${PREFIX}_model.txt
r.out.gdal input=${PREFIX}_residual output=${INDIR}/${PREFIX}_residual.tif
r.out.gdal input=${PREFIX}_estimate output=${INDIR}/${PREFIX}_estimate.tif
