#!/bin/bash
## Processing Sentinel2 data per scene
## Author: Sajid Pareeth, 2017
## For 10m, use bands - 2,3,4,8
## For 20m, use bands - 5,6,7,8a,11,12
## For 60m, use bands - 1,9,10
## Note: S2A Band 8A = L8 Band 5 (NIR)

cleanup()
{
  m=$1
  # we clean up in the GRASS DB and the scratch space:
  g.remove type=rast name=${m}_B01,${m}_B02,${m}_B03,${m}_B04,${m}_B05,${m}_B06,${m}_B07,${m}_B08,${m}_B8A,${m}_B09,${m}_B10,${m}_B11,${m}_B12 -f
}

# what to do in case of user break:
exitprocedure()
{
 g.message -e 'User break!'
 unset GRASS_BATCH_JOB
 cleanup ${BASE}
 rm -rf ${GRASSDB}/S2_UTM_${CODE}
 exit 1
}
# shell check for user break (signal list: trap -l)
trap "exitprocedure" 2 3 15

if [ -z "$GISBASE" ] ; then
    echo "Please run S2_TileProcess_v1.sh to run this program." >&2
    exit 1
fi
export GRASS_OVERWRITE=1
export GRASS_MESSAGE_FORMAT=plain  # percent output as 0..1..2..
# setting environment, so that awk works properly in all languages
unset LC_ALL
LC_NUMERIC=C
export LC_NUMERIC

cd ${INDIR}/${SCN}
for YR in `ls -d *`; do
	cd ${INDIR}/${SCN}/${YR}
		for M in `ls -d *`; do
			MM=`echo $M | awk '{ printf("%02d\n", $1) }'`
			cd ${INDIR}/${SCN}/${YR}/${M}
			for D in `ls -d *`; do
				DD=`echo $D | awk '{ printf("%02d\n", $1) }'`
				cd ${INDIR}/${SCN}/${YR}/${M}/${D}
					for N in `ls -d *`; do
						BASE=S2A_${YR}_${MM}_${DD}_T${SCN}_n${N}
						cd ${INDIR}/${SCN}/${YR}/${M}/${D}/${N}
						if [[ ! -z ${DISTRICT} ]]; then
							VOUT=`echo ${DISTRICT}|rev|cut -d/ -f1|rev|cut -d. -f1`
							v.in.ogr input=${DISTRICT} output=${VOUT}
							g.region vect=${VOUT} res=10 -a
							r.mask vect=${VOUT}
						else
							echo "Processing the entire tile"
							IMG=`ls *B02.jp2`
							OUT="${BASE}_B02"
							r.in.gdal input=${IMG} output=${OUT} memory=1000 -e
							g.region rast=${OUT} res=10 -a
							r.mapcalc "${OUT} = ${OUT} * 1.0"
						fi
						for i in "03" "04" "08"; do
							IMG=`ls *B${i}.jp2`
							OUT="${BASE}_B${i}"
							r.in.gdal input=${IMG} output=${OUT} memory=1000
							r.mapcalc "${OUT} = ${OUT} * 1.0"
						done
						i.group group=${BASE}_10m input=${BASE}_B02,${BASE}_B03,${BASE}_B04,${BASE}_B08
						g.region rast=${BASE}_B02 res=20 -a
						for i in "05" "06" "07" "8A" "11" "12"; do
							IMG=`ls *B${i}.jp2`
							OUT="${BASE}_B${i}"
							r.in.gdal input=${IMG} output=${OUT} memory=1000
							r.mapcalc "${OUT} = ${OUT} * 1.0"
						done
						i.group group=${BASE}_20m input=${BASE}_B05,${BASE}_B06,${BASE}_B07,${BASE}_B8A,${BASE}_B11,${BASE}_B12
						g.region rast=${BASE}_B02 res=60 -a
						for i in "01" "09" "10"; do
							IMG=`ls *B${i}.jp2`
							OUT="${BASE}_B${i}"
							r.in.gdal input=${IMG} output=${OUT} memory=1000
							r.mapcalc "${OUT} = ${OUT} * 1.0"
						done
						i.group group=${BASE}_60m input=${BASE}_B01,${BASE}_B09,${BASE}_B10
						g.region rast=${BASE}_B02 res=10 -a
						## DATA FUSION
						i.fusion.hpf -l -c pan=${BASE}_B04 msx=${BASE}_B05 center=high modulation=max trim=0.0 --o
						i.fusion.hpf -l -c pan=${BASE}_B04 msx=${BASE}_B11 center=high modulation=max trim=0.0 --o
						i.fusion.hpf -l -c pan=${BASE}_B04 msx=${BASE}_B12 center=high modulation=max trim=0.0 --o
						i.fusion.hpf -l -c pan=${BASE}_B08 msx=${BASE}_B06 center=high modulation=max trim=0.0 --o
						i.fusion.hpf -l -c pan=${BASE}_B08 msx=${BASE}_B07 center=high modulation=max trim=0.0 --o
						i.fusion.hpf -l -c pan=${BASE}_B08 msx=${BASE}_B8A center=high modulation=max trim=0.0 --o
						##INDICES
						r.mapcalc "${BASE}_NDVI_B0804 = ((${BASE}_B08 - ${BASE}_B04)/(${BASE}_B08 + ${BASE}_B04)) * 1.0"
						r.mapcalc "${BASE}_NDWI_B8A11_hpf = ((${BASE}_B8A.hpf - ${BASE}_B11.hpf)/(${BASE}_B8A.hpf + ${BASE}_B11.hpf)) * 1.0"
						#create composites
						i.colors.enhance red=${BASE}_B04 green=${BASE}_B03 blue=${BASE}_B02 strength=95
						r.composite red=${BASE}_B04 green=${BASE}_B03 blue=${BASE}_B02 output=${BASE}_comp_432
						i.colors.enhance red=${BASE}_B8A.hpf green=${BASE}_B04 blue=${BASE}_B03 strength=95
						r.composite red=${BASE}_B8A.hpf green=${BASE}_B04 blue=${BASE}_B03  output=${BASE}_comp_8A43
						##Outputs tifs
						r.mask -r
						i.group group=${BASE}_spectral input=${BASE}_B02,${BASE}_B03,${BASE}_B04,${BASE}_B05.hpf,${BASE}_B06.hpf,${BASE}_B07.hpf,${BASE}_B08,${BASE}_B8A.hpf,${BASE}_B11.hpf,${BASE}_B12.hpf
						mkdir -p ${OUTDIR}/${SCN}/${BASE}/SPECTRAL ${OUTDIR}/${SCN}/${BASE}/COMPOSITES ${OUTDIR}/${SCN}/${BASE}/INDICES
						r.out.gdal in=${BASE}_spectral out=${OUTDIR}/${SCN}/${BASE}/SPECTRAL/${BASE}_spectral.tif
						r.out.gdal in=${BASE}_comp_432 out=${OUTDIR}/${SCN}/${BASE}/COMPOSITES/${BASE}_comp_432.tif
						r.out.gdal in=${BASE}_comp_8A43 out=${OUTDIR}/${SCN}/${BASE}/COMPOSITES/${BASE}_comp_8A43.tif
						r.out.gdal in=${BASE}_NDVI_B0804 out=${OUTDIR}/${SCN}/${BASE}/INDICES/${BASE}_NDVI_B0804.tif
						r.out.gdal in=${BASE}_NDWI_B8A11_hpf out=${OUTDIR}/${SCN}/${BASE}/INDICES/${BASE}_NDWI_B8A11.tif
						cleanup ${BASE}
					done
			done
		done
done
MAPS=`g.list rast map=. pattern=*NDVI*`
CNT=`g.list rast map=. pattern=S2A*NDVI*|wc -l`
rm -f ${OUTDIR}/${SCN}/Report_processing_${SCN}.txt
echo "A total of ${CNT} datasets were processed for the tile ${SCN}." >> ${OUTDIR}/${SCN}/Report_processing_${SCN}.txt
echo "Bands 5,6,7,8A,11,12 with 20m resolution were fused to 10m, and further processing were done at 10m" >> ${OUTDIR}/${SCN}/Report_processing_${SCN}.txt
echo "The folder SPECTRAL consists of a tif file: Stack of bands 2,3,4,5,6,7,8,8A,11,12 in that order" >> ${OUTDIR}/${SCN}/Report_processing_${SCN}.txt
echo "The tif file in the 'SPECTRAL' folder will be used as input to Classification step" >> ${OUTDIR}/${SCN}/Report_processing_${SCN}.txt
echo "The 'INDICES' folder has NDVI and NDWI of each scene in tif format" >> ${OUTDIR}/${SCN}/Report_processing_${SCN}.txt
echo "The 'COMPOSITES' folder has true color (4:3:2), and False color (8A:4:3) composites in tif format" >> ${OUTDIR}/${SCN}/Report_processing_${SCN}.txt
echo "The following maps are processed for the scene:${SCN}:" >> ${OUTDIR}/${SCN}/Report_processing_${SCN}.txt
echo "${MAPS}" >> ${OUTDIR}/${SCN}/Report_processing_${SCN}.txt
