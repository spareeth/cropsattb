#!/bin/bash
## Author: Sajid Pareeth, 2017
## accuracy assessment using kappa statistics, output a text file with all the required stats
cleanup()
{
  m=$1
  # we clean up in the GRASS DB and the scratch space:
  g.remove type=rast name=${m}_B1,${m}_B2,${m}_B3,${m}_B4,${m}_B5,${m}_B6,${m}_B7,${m}_B8,${m}_B9 -f
}

# what to do in case of user break:
exitprocedure()
{
 g.message -e 'User break!'
 unset GRASS_BATCH_JOB
 rm -rf ${TMP} ${GRASSDB}/TMP_${CODE}
 cleanup
 exit 1
}
# shell check for user break (signal list: trap -l)
trap "exitprocedure" 2 3 15


if [ -z "$GISBASE" ] ; then
    echo "Please run G_ClassAccuracy.sh to run this program." >&2
    exit 1
fi
export GRASS_OVERWRITE=1
export GRASS_MESSAGE_FORMAT=plain  # percent output as 0..1..2..
# setting environment, so that awk works properly in all languages
unset LC_ALL
LC_NUMERIC=C
export LC_NUMERIC

OUT=`echo ${INPUT}|cut -d. -f1`
OUTREF=`echo ${REF}|cut -d. -f1`
r.in.gdal input=${INDIR}/${INPUT} output=${OUT}
g.region rast=${OUT}
v.in.ogr input=${INDIR}/${REF} output=${OUTREF}
v.to.rast input=${OUTREF} output=${OUTREF} use=attr attribute_column=id
r.kappa classification=${OUT} reference=${OUTREF} output=${INDIR}/${OUT}_accuracy.txt
