#!/bin/bash
## Sajid Pareeth, 2017
## This script checks whether the downloaded Sentinel1 scene from Scihub covers the required bbox entirely or not
## Run S1_ValidScenes.sh to execute this script

exitprocedure()
{
 g.message -e 'User break!'
 unset GRASS_BATCH_JOB
 rm -rf ${TMP} ${GRASSDB}/TMP_${CODE}
 exit 1
}
# shell check for user break (signal list: trap -l)
trap "exitprocedure" 2 3 15

if [ -z "$GISBASE" ] ; then
    echo "Please run S1_ValidScenes.sh to run this program." >&2
    exit 1
fi
export GRASS_OVERWRITE=1
export GRASS_MESSAGE_FORMAT=plain  # percent output as 0..1..2..
# setting environment, so that awk works properly in all languages
unset LC_ALL
LC_NUMERIC=C
export LC_NUMERIC
cd ${INDIR}
rm -f ${INDIR}/VALID_SCENES.csv
for FOLDER in `ls -d */`; do
	NAME=`echo $FOLDER|cut -f1 -d.`
	cd ${INDIR}/${FOLDER}preview
	rm -f pts.txt pts_utm.txt
	i=$(grep -oPm1 "(?<=<coordinates>)[^<]+" map-overlay.kml)
	echo $i|sed 's/ /\n/g'|sed 's/,/ /g' >> pts.txt
	m.proj -i in=pts.txt out=pts_utm.txt
	v.in.ascii input=pts_utm.txt output=pts
	v.hull input=pts output=pts_hull cats=1
	VOUT=`echo ${BBOX}|rev|cut -d/ -f1|rev|cut -d. -f1`
	v.in.ogr input=${BBOX} output=${VOUT}
	v.select ainput=${VOUT} binput=pts_hull oper=within out=a_within_b -c
	unset boundaries
	rm -f pts.txt pts_utm.txt
	eval `v.info a_within_b -t`
	Y=$boundaries
	if [ ${Y} -eq 1 ]; then
		echo "The requested bbox is entirely inside the scene: ${NAME}"
		g.rename vector=pts_hull,${NAME}_bound
		POL=`ls ${INDIR}/${FOLDER}measurement|wc -l`
		ACQ=$(grep -oPm1 "(?<=<safe:startTime>)[^<]+" ${INDIR}/${FOLDER}manifest.safe|sed 's|[-:.,]||g')
		DATE=`echo ${ACQ}|cut -c1-8`
		TIME=`echo ${ACQ}|cut -c10-15`
		echo "${NAME},${DATE},${TIME},${POL}" >> ${INDIR}/VALID_SCENES.csv
	else
		echo "The requested bbox is not entirely inside the scene, ignoring ${NAME}"
		continue
	fi
done
