#!/bin/bash
## Importing sentinel 1 SAR ###
## extract the region, identify time of acquisition, single or double pole
## preprocessing using OTB - Calibrate, despeckle, ortho-rectify
## Run inside GRASS mapset S1_bihar
g.gisenv set=DEBUG=0
# what to do in case of user break:
exitprocedure()
{
 g.message -e 'User break!'
 unset GRASS_BATCH_JOB
 rm -f *_calib*
 rm -rf ${TMP} ${GRASSDB}/TMP_${CODE}
 exit 1
}

# shell check for user break (signal list: trap -l)
trap "exitprocedure" 2 3 15

if [ -z "$GISBASE" ] ; then
	echo "Please run S1_Process.sh to run this program." >&2
    exit 1
fi
export GRASS_OVERWRITE=1
export GRASS_MESSAGE_FORMAT=plain  # percent output as 0..1..2..
# setting environment, so that awk works properly in all languages
unset LC_ALL
LC_NUMERIC=C
export LC_NUMERIC

VOUT=`echo ${BBOX}|rev|cut -d/ -f1|rev|cut -d. -f1`
v.in.ogr input=${BBOX} output=${VOUT}
g.region vect=${VOUT} res=10 -a
cd ${INDIR}
for FOLDER in `ls -d */`; do
	DATA="${FOLDER}measurement"
	ACQ=$(grep -oPm1 "(?<=<safe:startTime>)[^<]+" ${INDIR}/${FOLDER}manifest.safe|sed 's|[-:.,]||g')
	DATE=`echo ${ACQ}|cut -c1-8`
	TIME=`echo ${ACQ}|cut -c10-15`
	NAME1=`echo $FOLDER|cut -d_ -f1-4`
	NAME2=`echo $FOLDER|cut -d_ -f7-8`
	cd ${DATA}
	for IN in `ls *.tiff`; do
		POL=`echo $IN|cut -d- -f4`
		OUT="${NAME1}_${POL}_${DATE}_${TIME}_${NAME2}"
		source otbenv.profile
		otbcli_SARCalibration -in ${IN} -lut sigma -out ${OUT}_calib.tiff
		otbcli_OrthoRectification -io.in ${OUT}_calib.tiff -opt.ram 3000 -io.out ${OUT}_calib_ortho.tiff -outputs.spacingx 10 -outputs.spacingy -10 -opt.gridspacing 40
		source ${HOME}/.bashrc
		eval `v.info ${VOUT} -g`
		gdalwarp -te ${west} ${south} ${east} ${north} ${OUT}_calib_ortho.tiff ${OUT}_calib_ortho_clip.tiff
		unset north south east west
		source otbenv.profile
		otbcli_Despeckle -in ${OUT}_calib_ortho_clip.tiff -filter lee -filter.lee.rad 1 -out ${OUT}_calib_ortho_clip_lee.tiff
		source ${HOME}/.bashrc
		r.in.gdal in=${OUT}_calib_ortho_clip_lee.tiff out=${OUT}_calib_ortho_clip_lee -o
		r.mapcalc "${OUT}_calib_ortho_clip_lee_db = 10 * log(${OUT}_calib_ortho_clip_lee, 10)"
		r.out.gdal input=${OUT}_calib_ortho_clip_lee_db output=${OUTDOR}/${OUT}_DB_output.tif
	done
	VH="${NAME1}_vh_${DATE}_${TIME}_${NAME2}_calib_ortho_clip_lee"
	VV="${NAME1}_vv_${DATE}_${TIME}_${NAME2}_calib_ortho_clip_lee"
	r.mapcalc "${OUT}_WaterLogged_output = if(${OUT}_calib_ortho_clip_lee < 0.025, 1, null())" --o
	r.out.gdal input=${OUT}_WaterLogged_output output=${OUTDOR}/${OUT}_WaterLogged_output.tif
	rm -f *_calib*
	cd ${INDIR}
done
