#!/bin/bash
## Author: Sajid Pareeth, 2017
## Script to develope multi linear model and estimate yield

set -a
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('indir', help='Master directory where X and Y variable maps are stored')
parser.add_argument('xi', help='Independent variable maps in tif format (eg: NDVI.tif,Precip.tif)')
parser.add_argument('y', help='Dependent variable, as a column in a shape file (eg: Bihar_taluks.shp)')
parser.add_argument('var', help='name of the column which explains the yi variable in the shape file (eg: yield)')
parser.add_argument('prefix', help='Prefix for the output files, Following outputs: Prefix_estim.tif, Prefix_resid.tif, Prefix_model.txt will be created')
parser.add_argument('grassdb', help='Path to grass data base (eg: /home/grassdata)')
parser.add_argument('crs', help='UTM zone of L8 tiles in EPSG code eg: epsg:32632')
EOF
echo required indir: "$INDIR"
echo required xi: "$XI"
echo required y: "$Y"
echo required var: "$VAR"
echo required prefix: "$PREFIX"
echo required grassdb: "$GRASSDB"
echo required crs: "$CRS"
CODE=`echo ${CRS}|cut -d: -f2`
# create new temporary location for the job, exit after creation of this location
grass -c ${CRS} ${GRASSDB}/TMP_${CODE} -e
# now we can use this new location and run the job defined via	GRASS_BATCH_JOB
LN=`which G_ModelYield`
LNK=`readlink ${LN}`
OLN=`echo ${LNK}|rev|cut -d/ -f3- |rev`
OLNK="${OLN}/Sub_scripts"

export GRASS_BATCH_JOB="${OLNK}/G_SubModelYield.sh"
grass -c ${GRASSDB}/TMP_${CODE}/tmp
unset GRASS_BATCH_JOB
TIME=`date`
echo "FINISHED processing at ${TIME}"
echo "${PREFIX}_estim.tif, ${PREFIX}_resid.tif, ${PREFIX}_model.txt are saved in ${INDIR}"
rm -rf ${TMP} ${GRASSDB}/TMP_${CODE}
set +a
