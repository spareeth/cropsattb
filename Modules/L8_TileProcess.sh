#!/bin/bash
## Processing Landsat 8 data per scene
## scenes applicable 141/42, 141/43 , 140/43
## Author: Sajid Pareeth, 2017
## For 15m pan, use band - 8
## For 30m, use bands - 1,2,3,4,5,6,7
## Note: S2A Band 8A = L8 Band 5 (NIR)

set -a
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('indir', help='Master directory where L8 tiles are saved')
parser.add_argument('outdir', help='Directory where processed data should be saved')
parser.add_argument('grassdb', help='Path to grass data base (eg: /home/grassdata)')
parser.add_argument('crs', help='UTM zone of L8 tiles in EPSG code eg: epsg:32632')
parser.add_argument('-d', '--district', help='optional, provide a shape file of district to do the entire processing masked to the district boundaries')
EOF
echo required indir: "$INDIR"
echo required indir: "$OUTDIR"
echo required grassdb: "$GRASSDB"
echo required crs: "$CRS"
echo optional district: "$DISTRICT"
mkdir -p ${OUTDIR}/.L8_temp
TMP="${OUTDIR}/.L8_temp"
PRN=`ls ${INDIR}/`
CODE=`echo ${CRS}|cut -d: -f2`
# create new temporary location for the job, exit after creation of this location
grass -c ${CRS} ${GRASSDB}/L8_UTM_${CODE} -e
# now we can use this new location and run the job defined via	GRASS_BATCH_JOB

LN=`which L8_TileProcess`
LNK=`readlink ${LN}`
OLN=`echo ${LNK}|rev|cut -d/ -f3- |rev`
OLNK="${OLN}/Sub_scripts"

for T in ${PRN}; do
	export GRASS_BATCH_JOB="${OLNK}/L8_SubTileProcess.sh"
	grass -c ${GRASSDB}/L8_UTM_${CODE}/${T}
	unset GRASS_BATCH_JOB
done
rm -rf ${GRASSDB}/L8_UTM_${CODE}
TIME=`date`
echo "FINISHED processing at ${TIME}"
echo "All outputs are in ${OUTDIR}"
set +a
