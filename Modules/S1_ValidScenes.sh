#!/bin/bash
## Sajid Pareeth, 2017
## This script checks whether the downloaded Sentinel1 scene from Scihub covers the required bbox entirely or not
## Require associated script - S1_SubValidScenes.sh

set -a
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('indir', help='Master directory where Sentinel 1 SAR scenes are stored')
parser.add_argument('bbox', help='Shape file representing the bounding box, for which the valid scenes has to be listed, For eg: district.shp')
parser.add_argument('grassdb', help='Path to grass data base (eg: /home/grassdata)')
parser.add_argument('crs', help='UTM zone of the bbox in EPSG code eg: epsg:32632')
EOF
echo required indir: "$INDIR"
echo required indir: "$BBOX"
echo required grassdb: "$GRASSDB"
echo required crs: "$CRS"
mkdir -p ${INDIR}/.S1_temp
TMP="${INDIR}/.S1_temp"
PRN=`ls ${INDIR}/`
CODE=`echo ${CRS}|cut -d: -f2`
# create new temporary location for the job, exit after creation of this location
grass -c ${CRS} ${GRASSDB}/TMP_${CODE} -e
# now we can use this new location and run the job defined via	GRASS_BATCH_JOB
LN=`which S1_ValidScenes`
LNK=`readlink ${LN}`
OLN=`echo ${LNK}|rev|cut -d/ -f3- |rev`
OLNK="${OLN}/Sub_scripts"

export GRASS_BATCH_JOB="${OLNK}/S1_SubValidScenes.sh"
grass -c ${GRASSDB}/TMP_${CODE}/tmp
unset GRASS_BATCH_JOB
TIME=`date`
echo "FINISHED processing at ${TIME}"
echo "List of valid scenes 'VALID_SCENES.csv' is in ${INDIR}"
rm -rf ${TMP} ${GRASSDB}/TMP_${CODE}
set +a
