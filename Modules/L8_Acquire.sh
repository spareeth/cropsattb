#!/bin/bash
## Author: Sajid Pareeth, 2017
## Automating the data acquisition from L8
## Downloading Landsat 8 data from Amazon cloud
## S3 bucket address: s3://landsat-pds/
## Pre-requisite: save the profile L8 using the command:
## aws configure --profile=L8

exitprocedure()
{
 g.message -e 'User break!'
 rm -rf ${TMP}
 #rm -rf ${OUTDIR}/*
 exit 1
}
# shell check for user break (signal list: trap -l)
trap "exitprocedure" 2 3 15

export AWS_DEFAULT_PROFILE=L8
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('outdir', help='Output directory to save L8 tiles')
parser.add_argument('pr', help='L8 paths/rows to download (eg: 147039,147040)')
parser.add_argument('years', help='Years for which data has to be downloaded (eg: 2015,2017)')
parser.add_argument('start', help='start date (eg: mm-dd)')
parser.add_argument('stop', help='stop date (eg: mm-dd)')
EOF
echo required outdir: "$OUTDIR"
echo required pr: "$PR"
echo required years: "$YEARS"
echo required start: "$START"
echo required stop: "$STOP"

mkdir -p ${OUTDIR}/.L8_temp
TMP="${OUTDIR}/.L8_temp"

PRN=`echo ${PR}|sed 's/,/ /g'`
YR=`echo ${YEARS}|sed 's/,/ /g'`
for T in ${PRN}; do
	mkdir -p ${OUTDIR}/${T}
	P=`echo ${T}|cut -c1-3`
	R=`echo ${T}|cut -c4-6`
	aws s3 ls s3://landsat-pds/L8/${P}/${R}/ > ${TMP}/L8_filenames_${P}_${R}.txt
	for Y in ${YR}; do
		ST=`date2doy.sh ${Y}-${START}`
		SP=`date2doy.sh ${Y}-${STOP}`
		for i in `seq ${Y}${ST} ${Y}${SP}`; do
			NAME=`cat ${TMP}/L8_filenames_${P}_${R}.txt|grep LC8${P}${R}${i}`
			if grep "LC8${P}${R}${i}" ${TMP}/L8_filenames_${P}_${R}.txt;
			then
				aws s3 cp s3://landsat-pds/L8/${P}/${R}/ ${OUTDIR}/${T} --exclude "*" --include "LC8${P}${R}${i}*" --recursive
			else
				echo "Checking for next available image in ${Y}, ${i}"
			fi
		done
	done
done
rm -rf ${TMP}
TIME=`date`
echo "Finished downloading the required data at $TIME"
echo "Check ${OUTDIR}"







