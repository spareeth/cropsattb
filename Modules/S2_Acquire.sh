#!/bin/bash
## Author: Sajid Pareeth, 2017
## Automating the data acquisition from S2A
## Downloading Sentinel2A data from Amazon cloud
## S3 bucket address: s3://sentinel-s2-l1c/
## Pre-requisite: save the profile S2A using the command:
## aws configure --profile=S2A
## Search for path row : https://remotepixel.ca/projects/satellitesearch.html
# what to do in case of user break:
exitprocedure()
{
 g.message -e 'User break!'
 rm -rf ${TMP}
 #rm -rf ${OUTDIR}/*
 exit 1
}
# shell check for user break (signal list: trap -l)
trap "exitprocedure" 2 3 15

export AWS_DEFAULT_PROFILE=S2A
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('outdir', help='Output directory to save S2A tiles')
parser.add_argument('scenes', help='S2A scene ids to download (eg: 43REN,43RFP,43RFN)')
parser.add_argument('years', help='Years for which data has to be downloaded (eg: 2015,2017)')
parser.add_argument('start', help='Start month (eg: 1,2,3...12), NOTE: starting month must be always less than stop month')
parser.add_argument('stop', help='Stop month (eg: 1,2,3...12)')
EOF
echo required outdir: "$OUTDIR"
echo required pr: "$SCENES"
echo required years: "$YEARS"
echo required start: "$START"
echo required stop: "$STOP"

mkdir -p ${OUTDIR}/.S2A_temp
TMP="${OUTDIR}/.S2A_temp"

SCN=`echo ${SCENES}|sed 's/,/ /g'`
YR=`echo ${YEARS}|sed 's/,/ /g'`
for T in ${SCN}; do
	G=`echo ${T}|cut -c1-2`
	P=`echo ${T}|cut -c3`
	R=`echo ${T}|cut -c4-5`
	for Y in ${YR}; do
		for i in `seq ${START} ${STOP}`; do
			mkdir -p ${OUTDIR}/${T}/${Y}/${i}
			CNT=`aws s3 ls s3://sentinel-s2-l1c/tiles/${G}/${P}/${R}/${Y}/${i}/|wc -l`
			if [[ ${CNT} -gt 0 ]];
			then
				aws s3 cp s3://sentinel-s2-l1c/tiles/${G}/${P}/${R}/${Y}/${i}/ ${OUTDIR}/${T}/${Y}/${i} --recursive
			else
				echo "No sentinel data available for the month ${i} of ${Y}"
			fi
		done
	done
done
rm -rf ${TMP}
TIME=`date`
echo "Finished downloading the required data at $TIME"
echo "Check ${OUTDIR}"
