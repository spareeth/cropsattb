#!/bin/bash
## Processing Landsat 8 data per scene
## scenes applicable 141/42, 141/43 , 140/43
## Author: Sajid Pareeth, 2017
## For 15m pan, use band - 8
## For 30m, use bands - 1,2,3,4,5,6,7
## Note: S2A Band 8A = L8 Band 5 (NIR)

set -a
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('indir', help='Master directory where classified image and the reference shape file are located')
parser.add_argument('input', help='Input classified image, .tif file')
parser.add_argument('ref', help='Reference shape file with ground truth data to validate. There must be "id" column with class ids. CRS of both classified image and reference shape file must be same')
parser.add_argument('grassdb', help='Path to grass data base (eg: /home/grassdata)')
parser.add_argument('crs', help='UTM zone of L8 tiles in EPSG code eg: epsg:32632')
EOF
echo required indir: "$INDIR"
echo required input: "$INPUT"
echo required ref: "$REF"
echo required grassdb: "$GRASSDB"
echo required crs: "$CRS"
CODE=`echo ${CRS}|cut -d: -f2`
# create new temporary location for the job, exit after creation of this location
grass -c ${CRS} ${GRASSDB}/TMP_${CODE} -e
# now we can use this new location and run the job defined via	GRASS_BATCH_JOB

LN=`which G_ClassAccuracy`
LNK=`readlink ${LN}`
OLN=`echo ${LNK}|rev|cut -d/ -f3- |rev`
OLNK="${OLN}/Sub_scripts"
export GRASS_BATCH_JOB="${OLNK}/G_SubClassAccuracy.sh"
grass -c ${GRASSDB}/TMP_${CODE}/tmp
unset GRASS_BATCH_JOB
TIME=`date`
echo "FINISHED processing at ${TIME}"
echo "Accuracy report is in ${INDIR}"
rm -rf ${TMP} ${GRASSDB}/TMP_${CODE}
set +a
