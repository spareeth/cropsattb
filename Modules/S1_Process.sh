#!/bin/bash
## Importing sentinel 1 SAR ###
## extract the region, identify time of acquisition, single or double pole
## preprocessing using OTB - Calibrate, despeckle, ortho-rectify
## Run inside GRASS mapset S1_bihar
set -a
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('indir', help='Master directory where Sentinel 1 SAR scenes are stored, after checking for the valid scenes')
parser.add_argument('outdir', help='Directory where processed data should be saved')
parser.add_argument('bbox', help='Shape file representing the bounding box, for which the Sentinel 1 scene will be cropped and processed')
parser.add_argument('grassdb', help='Path to grass data base (eg: /home/grassdata)')
parser.add_argument('crs', help='UTM zone of the bbox in EPSG code eg: epsg:32632')
EOF
echo required indir: "$INDIR"
echo required outdir: "$OUTDIR"
echo required bbox: "$BBOX"
echo required grassdb: "$GRASSDB"
echo required crs: "$CRS"
mkdir -p ${INDIR}/.S1_temp
TMP="${INDIR}/.S1_temp"
PRN=`ls ${INDIR}/`
CODE=`echo ${CRS}|cut -d: -f2`
# create new temporary location for the job, exit after creation of this location
grass -c ${CRS} ${GRASSDB}/TMP_${CODE} -e
# now we can use this new location and run the job defined via	GRASS_BATCH_JOB
LN=`which S1_Process`
LNK=`readlink ${LN}`
OLN=`echo ${LNK}|rev|cut -d/ -f3- |rev`
OLNK="${OLN}/Sub_scripts"

export GRASS_BATCH_JOB="${OLNK}/S1_SubProcess.sh"
grass -c ${GRASSDB}/TMP_${CODE}/tmp
unset GRASS_BATCH_JOB
TIME=`date`
echo "FINISHED processing at ${TIME}"
echo "Please check for outputs in ${OUTDIR}"
rm -rf ${TMP} ${GRASSDB}/TMP_${CODE}
set +a
