#!/bin/bash
## Processing Sentinel2 data per scene
## Author: Sajid Pareeth, 2017
## For 10m, use bands - 2,3,4,8
## For 20m, use bands - 5,6,7,8a,11,12
## For 60m, use bands - 1,9,10
## Note: S2A Band 8A = L8 Band 5 (NIR)

set -a
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('indir', help='Master directory where S2 scenes are saved')
parser.add_argument('outdir', help='Directory where processed data should be saved')
parser.add_argument('grassdb', help='Path to grass data base (eg: /home/grassdata)')
parser.add_argument('crs', help='UTM zone of S2 scenes in EPSG code eg: epsg:32632')
parser.add_argument('-d', '--district', help='optional, provide a shape file of district to do the entire processing masked to the district boundaries')
EOF
echo required indir: "$INDIR"
echo required indir: "$OUTDIR"
echo required grassdb: "$GRASSDB"
echo required crs: "$CRS"
echo optional district: "$DISTRICT"
mkdir -p ${OUTDIR}/.S2_temp
TMP="${OUTDIR}/.S2_temp"
PRN=`ls ${INDIR}/`
CODE=`echo ${CRS}|cut -d: -f2`
# create new temporary location for the job, exit after creation of this location
grass -c ${CRS} ${GRASSDB}/S2_UTM_${CODE} -e
# now we can use this new location and run the job defined via	GRASS_BATCH_JOB

LN=`which S2_TileProcess`
LNK=`readlink ${LN}`
OLN=`echo ${LNK}|rev|cut -d/ -f3- |rev`
OLNK="${OLN}/Sub_scripts"

for SCN in ${PRN}; do
	export GRASS_BATCH_JOB="${OLNK}/S2_SubTileProcess.sh"
	grass -c ${GRASSDB}/S2_UTM_${CODE}/${SCN}
	unset GRASS_BATCH_JOB
done
rm -rf ${GRASSDB}/S2_UTM_${CODE}
TIME=`date`
echo "FINISHED processing at ${TIME}"
echo "All outputs are in ${OUTDIR}"
set +a
