#!/bin/bash
## Train classifier based on OTB toolbox
## RF and SVM only
## Author: Sajid Pareeth, 2017
set -a
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('indir', help='Master directory where L8 stacks and training samples are saved. Keep both of them in the same folder.')
parser.add_argument('instack', help='Input stacked L8 bands in tif format, output(SPECTRAL folder) from "L8_TileProcess". If multiple date images, give for eg: L8_date1.tif,L8_date2.tif,..etc.')
parser.add_argument('insample', help='Input training samples in shape format. If multiple date images, give for eg: Tr_date1.shp,Tr_date2.shp,...etc. REMEMBER to give samples in the same order as stack images ')
parser.add_argument('size', help='Maximum sample size of the training sites to be considered for the model development. Optimal is 20', default=20, type=int)
parser.add_argument('classifier', help='Classifier to use for model development. Two types are supported: "rf" and "svm"')
parser.add_argument('outdir', help='Directory where output model should be saved.')
parser.add_argument('prefix', help='Prefix to the output files')
EOF
echo required indir: "$INDIR"
echo required instack: "$INSTACK"
echo required insample: "$INSAMPLE"
echo required size: "$SIZE"
echo required classifier: "$CLASSIFIER"
echo required outdir: "$OUTDIR"
echo required prefix: "$PREFIX"

mkdir -p ${OUTDIR}/.L8_temp
TMP="${OUTDIR}/.L8_temp"
IN=`echo ${INSTACK}|sed 's/,/ /g'`
TR=`echo ${INSAMPLE}|sed 's/,/ /g'`
trees=100
rand=1234

cd ${INDIR}
source otbenv.profile
otbcli_ComputeImagesStatistics -il ${IN} -out ${OUTDIR}/${PREFIX}_${CLASSIFIER}_stat.xml

if [[ ${CLASSIFIER} == "rf" ]];
	then
		otbcli_TrainImagesClassifier -io.il ${IN} -io.vd ${TR} -io.imstat ${OUTDIR}/${PREFIX}_${CLASSIFIER}_stat.xml -sample.mv ${SIZE} -sample.mt ${SIZE} -sample.vtr 0.5 -sample.vfn id -classifier rf -classifier.rf.max 25 -classifier.rf.min 25 -classifier.rf.nbtrees ${trees} -rand ${rand} -io.out ${OUTDIR}/${PREFIX}_rf_model.txt -io.confmatout ${OUTDIR}/${PREFIX}_rf_conf_matrix.csv|tee ${OUTDIR}/${PREFIX}_${CLASSIFIER}_output.txt
	else
		otbcli_TrainImagesClassifier -io.il ${IN} -io.vd ${TR} -io.imstat ${OUTDIR}/${PREFIX}_${CLASSIFIER}_stat.xml -sample.mv ${SIZE} -sample.mt ${SIZE} -sample.vtr 0.5 -sample.vfn id -classifier libsvm -classifier.libsvm.k sigmoid -classifier.libsvm.c 1 -classifier.libsvm.opt 1 -rand ${rand} -io.out ${OUTDIR}/${PREFIX}_svm_model.txt -io.confmatout ${OUTDIR}/${PREFIX}_svm_conf_matrix.csv|tee ${OUTDIR}/${PREFIX}_${CLASSIFIER}_output.txt
fi
source ${HOME}/.bashrc
cd ${HOME}
echo "${CLASSIFIER} model is created at ${OUTDIR}"
rm -rf ${TMP}
