#!/bin/bash
## Classifier based on OTB toolbox
## RF and SVM models as input
## Author: Sajid Pareeth, 2017

set -a
source $(dirname $0)/argparse
argparse "$@" <<EOF || exit 1
parser.add_argument('indir', help='Master directory where L8 stack is saved')
parser.add_argument('modeldir', help='Directory where RF or SVM models are saved; output from L8_TrainClassifier')
parser.add_argument('prefix', help='Prefix to the model file (eg: "Prefix"_model.txt)')
parser.add_argument('input', help='Input single date stacked L8 bands in tif format')
parser.add_argument('classifier', help='Classifier to use for model development. Two types are supported: "rf" and "svm"')
parser.add_argument('outdir', help='Directory where output classified image should be saved.')
parser.add_argument('-m', '--mask', help='optional, provide NRSC LULC map to mask out the Evergreen(8) and Deciduous(9) forest from the final crop map. NOTE: Make sure the NRSC LULC map is in the same UTM CRS')
EOF
echo required indir: "$INDIR"
echo required modeldir: "$MODELDIR"
echo required prefix: "$PREFIX"
echo required input: "$INPUT"
echo required classifier: "$CLASSIFIER"
echo required outdir: "$OUTDIR"
echo optional mask: "$MASK"

mkdir -p ${OUTDIR}/.L8_temp
TMP="${OUTDIR}/.L8_temp"

OUT=`echo ${INPUT}|cut -d. -f1`
source otbenv.profile
otbcli_ComputeImagesStatistics -il ${INDIR}/${INPUT} -out ${OUTDIR}/${OUT}_stat.xml
otbcli_ImageClassifier -in ${INDIR}/${INPUT} -imstat ${OUTDIR}/${OUT}_stat.xml -model ${MODELDIR}/${PREFIX}_${CLASSIFIER}_model.txt -out ${OUTDIR}/${OUT}_${CLASSIFIER}_class.tif
source ${HOME}/.bashrc
##To mask out the forest area based on NRSC LULC map
## NEED NUMPY Python library
if [[ ! -z ${MASK} ]]; then
	gdal_calc.py -A ${MASK} --outfile=${TMP}/tmpReclass.tif --calc="1*(logical_or(A<8,A>9))" --overwrite
	gdaltindex ${TMP}/clipper.shp ${OUTDIR}/${OUT}_${CLASSIFIER}_class.tif
	gdalwarp -cutline ${TMP}/clipper.shp -tr 15 15 -crop_to_cutline ${TMP}/tmpReclass.tif ${TMP}/tmpReclass_clip.tif
	gdal_calc.py -A ${OUTDIR}/${OUT}_${CLASSIFIER}_class.tif -B ${TMP}/tmpReclass_clip.tif --outfile=${OUTDIR}/${OUT}_${CLASSIFIER}_class_masked.tif --calc="(A*B)" --overwrite
else
	echo "No Forest mask provided"
fi
rm -rf ${TMP}
cd ${HOME}
echo "Classified image is created at ${OUTDIR}"

