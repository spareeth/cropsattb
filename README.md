# README #

### What is this repository for? ###

* CropsatTB is command line toolbox to process Landsat8, Sentinel2A and Sentinel 1 time series data to classify crop types over South Asia using machine learning algorithms. The tools are wrap up to existing open source libraries like Orfeo Toolbox and Grass GIS facilitating easy management of bulk data and simplifying the process chain by automating the steps as much as possible. The Main developer of this toolbox is Sajid Pareeth, who can be contact via email: spareeth@gmail.com
* Version 1.0
* The toolbox is developed under funding from International Water Management Institute (IWMI), Sri Lanka.

### How do I install CropsatTB? ###

* [Meeting requirements](https://bitbucket.org/spareeth/cropsattb/wiki/Prerequisites)
* [Installation](https://bitbucket.org/spareeth/cropsattb/wiki/Installation)

### How do I use the CropsatTB modules ###
* [Introduction to CropsatTB modules](https://bitbucket.org/spareeth/cropsattb/wiki/Introduction)
* [How to execute CropsatTB Modules](https://bitbucket.org/spareeth/cropsattb/wiki/CropsatTB%20tutorial)

### Do you have suggestions, issues, or found bugs? ###

* Please use issues on the left side menu to register an issue

Any questions, email at spareeth@gmail.com